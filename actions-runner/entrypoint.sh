#!/bin/sh

token_url="https://api.github.com/repos/${GITHUB_OWNER}/${GITHUB_REPOSITORY}/actions/runners"

REGISTRATION_TOKEN=$(curl -sX POST -H "Accept: application/vnd.github.v3+json" -u "${GITHUB_OWNER}:${GITHUB_PERSONAL_TOKEN}" "${token_url}/registration-token" | jq .token --raw-output)

FLAGS=""

[ ! -z "${RUNNER_LABELS}" ] && FLAGS="${FLAGS} --labels ${RUNNER_LABELS}"
[ -n "${EPHEMERAL}" ] && FLAGS="${FLAGS} --ephemeral"

./config.sh \
    --name "$(hostname)" \
    --token "${REGISTRATION_TOKEN}" \
    --url "https://github.com/${GITHUB_OWNER}/${GITHUB_REPOSITORY}" \
    --work "${RUNNER_WORKDIR}" \
    --unattended \
    --disableupdate \
    --replace ${FLAGS}

remove() {
    DEREGISTER_TOKEN=$(curl -sX POST -H "Accept: application/vnd.github.v3+json" -u "${GITHUB_OWNER}:${GITHUB_PERSONAL_TOKEN}" "${token_url}/remove-token" | jq .token --raw-output)
    ./config.sh remove --unattended --token "${DEREGISTER_TOKEN}"
}

trap 'remove; exit 130' INT
trap 'remove; exit 143' TERM

./run.sh "$*" &

wait $!
